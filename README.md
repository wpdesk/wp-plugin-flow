[![pipeline status](https://gitlab.com/wpdesk/wp-plugin-flow/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-plugin-flow/commits/master) 
Integration: [![coverage report](https://gitlab.com/wpdesk/wp-plugin-flow/badges/master/coverage.svg?job=integration+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-plugin-flow/commits/master)
Unit: [![coverage report](https://gitlab.com/wpdesk/wp-plugin-flow/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-plugin-flow/commits/master)

wp-plugin-flow
====================
